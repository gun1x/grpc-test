package main

import (
	"context"
	"log"
	"strings"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
)

func main() {
	conn, err := grpc.Dial(
		"127.0.0.1:11111",
		grpc.WithInsecure(),
		grpc.WithKeepaliveParams(keepalive.ClientParameters{
			Time:    10 * time.Second,
			Timeout: 5 * time.Second,
		}),
		grpc.WithDisableRetry(),
	)
	if err != nil {
		panic(err)
	}
	c := NewTestServiceClient(conn)

	count := 0
	for {
		log.Println("STARTING NEW STREAM")
		stream, err := c.TestServerToClient(context.Background(), &TestMessage{})
		for {
			log.Println("connection state:", conn.GetState())
			count++
			if err != nil {
				log.Println("got client error:", err)
				log.Println("connection state on client error:", conn.GetState())
				break
			}
			response, streamError := stream.Recv()
			if streamError != nil {
				log.Println("Got stream error:", streamError)
				log.Println("connection state on stream error:", conn.GetState())
				break
			}
			log.Println("got response:", strings.Split(response.GetResponse(), " ")[1])
		}
		time.Sleep(11 * time.Second)
	}
}
