For both client and server, use `GODEBUG=http2debug=2 go run .` to test behavior.

You should have the client in one terminal, the server in the other and preferably another terminal with watch `'ss -taenp | grep 11111'`
