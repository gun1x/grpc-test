#!/bin/bash
export PATH="$PATH:$(go env GOPATH)/bin"
protoc --go_out=plugins=grpc:. --go_opt=paths=source_relative test.proto
